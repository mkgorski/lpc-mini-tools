CC     = gcc
Q	   = @./quiet "$@"
CFLAGS = -Os --std=gnu99

LPC_SUM = lpc-sum
LPC_WAIT_FOR = lpc-wait-for

.PHONY: all clean

all:
	$(CC) $(CFLAGS) -o $(LPC_SUM) $(LPC_SUM).c
	$(CC) $(CFLAGS) -o $(LPC_WAIT_FOR) $(LPC_WAIT_FOR).c

clean:
	@rm $(LPC_SUM) $(LPC_WAIT_FOR)

release: $(LPC_SUM) $(LPC_WAIT_FOR)
	cp $(LPC_SUM) $(LPC_WAIT_FOR) ~/.bin 






