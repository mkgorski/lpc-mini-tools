#include <stdio.h>
#include <unistd.h>

void print_usage(char *name)
{
    printf("%s - wait until specified file is created\n", name);
    printf("Usage: %s file\n", name);
}

int main(int argc, char *argv[])
{
    int rc = -1;

    if (argc != 2) {
        print_usage(argv[0]);
        return -1;
    }

    printf("Waiting for %s\n", argv[1]);
    while(rc < 0) {
        sleep(1);
        rc = access(argv[1], R_OK);
    }

    return 0;
}
