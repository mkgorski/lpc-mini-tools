#include <stdio.h>
#include <stdint.h>

void print_usage(char *name)
{
    printf("%s - calculate and write checksum for NXP LPC microcontrollers\n", name);
    printf("Usage: %s file\n", name);
}

int main(int argc, char *argv[]) 
{
    FILE *f;
    uint32_t ptr[8];
    uint32_t sum = 0;

    if (argc != 2) {
        print_usage(argv[0]);
        return -1;
    }

    f = fopen(argv[1], "r+b");
    if (!f) {
        perror ("Cannot open file");
        return -1;
    }

    fread(ptr, 4, 8, f);
    for (int i=0; i<8; i++)
        sum += ptr[i];

    ptr[7] = -sum;
    fseek(f, 0, SEEK_SET);
    fwrite(ptr, 4, 8, f);
    fclose(f);

    return 0;
}